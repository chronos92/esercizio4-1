//
//  ViewController2.swift
//  Esercizio4
//
//  Created by Manuel Ceschi on 11/07/18.
//  Copyright © 2018 Manuel Ceschi. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {

    @IBOutlet var txt_nome: UITextField!
    @IBOutlet var txt_cognome: UITextField!
    @IBOutlet var txt_telefono: UITextField!
    
    @IBOutlet var btn_salvataggio: UIButton!
    
    static var contatore: Int = 0
    
    var dati_lable_in: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // se valori_lable è != nil lo salva in val_lable e imposta la lable
        if let val_lable_in = dati_lable_in {
            txt_nome.text = val_lable_in.nome
            txt_cognome.text = val_lable_in.cognome
            txt_telefono.text = val_lable_in.telefono
        }
        
    }
    
    
    @IBAction func btn_salvataggio (_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Alert", message: "Confermi il salvataggio?", preferredStyle: .alert)
        
        let yes_action = UIAlertAction(title: "SI", style: .default) { [unowned self] (_) in
            
            if let dati = self.dati_lable_in
            {
                dati.nome = self.txt_nome.text!
                dati.cognome = self.txt_cognome.text!
                dati.telefono = self.txt_telefono.text!
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "utente modificato"), object: dati)
                
            }
            
            else {
            // salvataggio dati nell'oggetto user
            var user: User = User (id: ViewController2.contatore, nome: self.txt_nome.text!, cognome: self.txt_cognome.text!, telefono: self.txt_telefono.text!)
                
            ViewController2.contatore+=1
            
            // invia i dati al navigation controller
            //il nome della notifica è "nuovo utente creato", il dato passato è user (di tipo object)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "nuovo utente creato"), object: user)
            }
            
            self.navigationController?.popViewController(animated: true)
            
        }
        
        let no_action = UIAlertAction(title: "NO", style: .default) { [unowned self] (_) in
        }
        
        alert.addAction(yes_action)
        alert.addAction(no_action)
        
        present(alert, animated: true, completion: nil)
        
        
    }

}
